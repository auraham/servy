# Notes

## 1

Create a new project

```
$ cd ~/git
$ mix new servy
$ cd servy
$ git init .
```

Update `Servy.ex`:

```elixir
defmodule Servy do
  def hello do
    "world"
  end
end
```

Start `iex`:

```
$ cd servy
$ iex -S mix
```

Run `Servy.hello()`:

```
iex(1)> Servy.hello
"world"
```

You can use `-r Servy` to recompile and reload the module in `iex`. However, if you change the namespace (e.g., `Servy` to `Servy.Name`), then `-r Servy` will not work. Instead, use `recompile()` inside `iex` to recompile the whole project.

Modify `servy.ex` as follows:

```
defmodule Servy.Main do
  def hello do
    :atom
  end
end

```

Use `recompile()` to reload the module in `iex`.

Update `servy.ex` one more time:

```elixir
defmodule Servy.Main do
  def hello(name) do
    "Hello, #{name}"
  end
end

IO.puts Servy.Main.hello("Elixir")
```


Use `recompile()` to reload the module in `iex`. This time, you will see a message:

```
iex(13)> recompile()
Compiling 1 file (.ex)
Hello, Elixir
:ok
```

---

**Note** There are other ways for running our code:

```
$ elixir lib/servy.ex
```

```
$ iex
$ c "lib/servy.ex"  # c helper function to compile and run
```

However, since an application often comprises a bunch of files, we recommend to use `$ iex -S mix` to start an `iex` session and compile the whole project at once:

```
$ iex -S mix
$ recompile()     # in case you change some ex/exs file or do some major refactoring, like changing namespaces
$ r Servy.Main    # in case you change this specific file
```

---


## 2

Let's create a file for our application. By convention, we need to create a subdirectory inside `lib` with the same name as the project, in this case, `servy`. Then, we can add application code insde `lib/servy`.

```
$ cd servy/lib
$ mkdir servy
$ cd servy
$ touch handler.ex
```